
const users = fetch("https://jsonplaceholder.typicode.com/users");
const todos = fetch("https://jsonplaceholder.typicode.com/todos");

users.then((res)=>res.json())
.catch((err)=>console.log(err.message))
.then((res)=>{
    console.log(res);
    return todos;
})
.then((response)=>response.json()).then((todos)=>{
    console.log(todos);
})
.catch((err)=>console.log(err.message));