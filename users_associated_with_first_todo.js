const todos = fetch("https://jsonplaceholder.typicode.com/todos");

const users = fetch("https://jsonplaceholder.typicode.com/uses");

let first_todo = {};

todos.then((response)=> response.json()).then((todo)=>{
    first_todo = {...todo[0]};
   
    return users;
}).catch((err)=>console.log(err))
.then((response)=>response.json())
.catch((err)=>console.log(err.message))
.then((users)=>{
    if(!Array.isArray(users)){
        throw new Error("Data is not correct");
    }
    const users_of_first_todo = users.filter((user)=>{
        return first_todo.userId === user.id
    });
    console.log(users_of_first_todo);
})
.catch((err)=> console.error(err.message));