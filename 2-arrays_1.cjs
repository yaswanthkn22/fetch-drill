const movies = [
  {
    title: "The Godfather",
    director: "Francis Ford Coppola",
    year: 1972,
    cast: ["Marlon Brando", "Al Pacino", "James Caan"],
    genre: ["Crime", "Drama"],
    rating: 9.2,
    duration: 175,
  },
  {
    title: "The Shawshank Redemption",
    director: "Frank Darabont",
    year: 1994,
    cast: ["Tim Robbins", "Morgan Freeman", "Bob Gunton"],
    genre: ["Drama"],
    rating: 9.3,
    duration: 142,
  },
  {
    title: "The Dark Knight",
    director: "Christopher Nolan",
    year: 2008,
    cast: ["Christian Bale", "Heath Ledger", "Aaron Eckhart"],
    genre: ["Action", "Crime", "Drama"],
    rating: 9.0,
    duration: 152,
  },
  {
    title: "Inception",
    director: "Christopher Nolan",
    year: 2010,
    cast: ["Leonardo DiCaprio", "Joseph Gordon-Levitt", "Ellen Page"],
    genre: ["Action", "Adventure", "Sci-Fi"],
    rating: 8.8,
    duration: 148,
  },
  {
    title: "The Matrix",
    director: "Lana Wachowski, Lilly Wachowski",
    year: 1999,
    cast: ["Keanu Reeves", "Laurence Fishburne", "Carrie-Anne Moss"],
    genre: ["Action", "Sci-Fi"],
    rating: 8.7,
    duration: 136,
  },
  {
    title: "Fight Club",
    director: "David Fincher",
    year: 1999,
    cast: ["Brad Pitt", "Edward Norton", "Helena Bonham Carter"],
    genre: ["Drama"],
    rating: 8.8,
    duration: 139,
  },
  {
    title: "Forrest Gump",
    director: "Robert Zemeckis",
    year: 1994,
    cast: ["Tom Hanks", "Robin Wright", "Gary Sinise"],
    genre: ["Drama", "Romance"],
    rating: 8.8,
    duration: 142,
  },
  {
    title: "Pulp Fiction",
    director: "Quentin Tarantino",
    year: 1994,
    cast: ["John Travolta", "Samuel L. Jackson", "Uma Thurman"],
    genre: ["Crime", "Drama"],
    rating: 8.9,
    duration: 154,
  },
  {
    title: "The Silence of the Lambs",
    director: "Jonathan Demme",
    year: 1991,
    cast: ["Jodie Foster", "Anthony Hopkins", "Lawrence A. Bonney"],
    genre: ["Crime", "Drama", "Thriller"],
    rating: 8.6,
    duration: 118,
  },
  {
    title: "The Usual Suspects",
    director: "Bryan Singer",
    year: 1995,
    cast: ["Kevin Spacey", "Gabriel Byrne", "Chazz Palminteri"],
    genre: ["Crime", "Mystery", "Thriller"],
    rating: 8.5,
    duration: 106,
  },
];



// 1. Write a function that takes in a genre and returns an array of movie titles that belong to that genre, sorted in descending order of rating.
function getMoviesByGenre(genre_item){
  const movies_by_genre = movies.filter((movie)=>movie.genre.includes(genre_item))
  .sort((movie1,movie2)=>{
      return movie2.rating - movie1.rating;
  })
  .map((movie)=>movie.title);

  console.log(movies_by_genre);

}
getMoviesByGenre("Action");

// expected output: 
// [
//   "The Dark Knight",
//   "Inception",
//   "The Matrix",
//   "The Lord of the Rings: The Return of the King",
//   "The Avengers",
//   "Terminator 2: Judgment Day"
// ]




// 2. Write a function that takes in an actor's name and returns an array of movie titles that the actor appeared in, sorted by year of release.

function getMoviesByActor(actor){
  const movie_by_actor = movies.filter((movie)=>movie.cast.includes(actor))
  .sort((movie1,movie2)=>{
    return movie1.year - movie2.year;
  })
  .map((movie)=>movie.title);

  console.log(movie_by_actor)
}

getMoviesByActor("Leonardo DiCaprio");


// expected output: 
// [
//   "Titanic",
//   "The Departed",
//   "The Wolf of Wall Street",
//   "The Revenant"
// ]


// 3. Write a function that takes in a director's name and returns an object with the director's name as -
// - a key and an array of movie titles that the director directed as the value.

function getMoviesByDirector(director){
  const movies_by_director = movies.reduce((dir_movies,curr_movie)=>{
    if(curr_movie.director === director){
      if(dir_movies[director]){
        dir_movies[director].push(curr_movie.title);
      }else {
        dir_movies[director] = [curr_movie.title];
      }
    }
    return dir_movies
  },{});

  console.log(movies_by_director);
}

getMoviesByDirector("Christopher Nolan");

// expected output: 
// {
//   "Christopher Nolan": [
//     "The Dark Knight",
//     "Inception",
//     "Interstellar",
//     "Dunkirk",
//     "Memento"
//   ]
// }


// 4. Write a function that takes in a year and returns an object with the year as a key and an array of movie 
// titles released in that year as the value, sorted in descending order of rating.

function getMoviesByYear(year){
  const movies_year = movies.filter((movie)=>movie.year === year)
  .sort((movie1 , movie2)=> movie2.rating - movie1.rating)
  .reduce((movies , curr_movie)=>{
    if(movies[year]){
      movies[year].push(curr_movie.title);
    }else {
      movies[year] = [curr_movie.title];
    }
    return movies;
  },{});
  console.log(movies_year);
}

getMoviesByYear(1994);

// expected output: 
// {
//   1994: [
//     "The Shawshank Redemption",
//     "Pulp Fiction",
//     "Forrest Gump"
//   ]
// }


// 5. Write a function that returns an array of objects, where each object contains a unique combination of director and genre. 
// Each object should have a "director" key and a "genre" key, and the values of these keys should be arrays of movie titles that
//  match the combination of director and genre. The movie titles should be sorted in descending order of rating.


function getMoviesByDirectorAndGenre(){

  const movies_by_director_genre = movies.reduce((movies_obj,curr_movie)=>{
    curr_movie.genre.forEach((gnr)=>{
      const filtered_director_gnr = movies_obj.filter((obj)=>{
        return obj.genre === gnr && obj.director === curr_movie.director
      });
      if(filtered_director_gnr.length != 0){
        filtered_director_gnr[0].movies.push(curr_movie.title);
      }else{
        let obj = {
          director : curr_movie.director,
          genre : gnr,
          movies : [curr_movie.title]
        }
        movies_obj.push(obj);
      }
    })

    return movies_obj;
  },[])

  console.log(movies_by_director_genre);
}

getMoviesByDirectorAndGenre();


// expected output: 
// [
//   {
//     director: "Christopher Nolan",
//     genre: "Action",
//     movies: ["The Dark Knight"]
//   },
//   {
//     director: "Christopher Nolan",
//     genre: "Thriller",
//     movies: ["Inception"]
//   },
//   {
//     director: "Christopher Nolan",
//     genre: "Sci-Fi",
//     movies: ["Interstellar"]
//   },
//   {
//     director: "Steven Spielberg",
//     genre: "Action",
//     movies: ["Jurassic Park"]
//   },
//   {
//     director: "Steven Spielberg",
//     genre: "Drama",
//     movies: ["Schindler's List"]
//   },
//   {
//     director: "Steven Spielberg",
//     genre: "Adventure",
//     movies: ["Indiana Jones and the Raiders of the Lost Ark"]
//   }
// ]
