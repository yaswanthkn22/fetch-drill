const users = fetch("https://jsonplaceholder.typicode.com/users");

// const user_details = fetch("https://jsonplaceholder.typicode.com/users?id=1");

users.then((response)=>response.json())
.catch((err)=>console.log(err.message))
.then((users)=>{
    const user_promises = [];
    users.forEach(user => {
        user_promises.push(fetch(`https://jsonplaceholder.typicode.com/users?id=${user.id}`)
        .then((response)=>response.json())
        .catch((err)=>console.log(err.message)));
    });
    return user_promises;
})
.then((user_promises)=>{
    Promise.all(user_promises)
    .then((user_details)=> console.log(user_details))
    .catch((err)=>console.log(err.message));
})
.catch((err)=>console.error(err.message));